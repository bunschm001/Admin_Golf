1. This application runs a server host for the Golfy Golfers project. 
2. This server will receive and distribute instructions/packets from and to its connected peers/clients.
3. This application will only function correctly if the system it's running on has portforwarded to itself on its router.
4. The clients are required to use the corresponding public IPv4-Address to connect succesfully to this server. For example: 80.127.228.175