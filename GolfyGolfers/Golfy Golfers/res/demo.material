material texturedUnlit
{
    // uniforms
    u_worldViewProjectionMatrix = WORLD_VIEW_PROJECTION_MATRIX

    // samplers
    sampler u_diffuseTexture
    {
        mipmap = true
        wrapS = REPEAT
        wrapT = REPEAT
        minFilter = LINEAR_MIPMAP_LINEAR
        magFilter = LINEAR
    }

    // render state
    renderState
    {
        cullFace = true
        depthTest = true
    }

    technique
    {
        pass 0
        {
            // shaders
            vertexShader = res/shaders/textured.vert
            fragmentShader = res/shaders/textured.frag
        }
    }
}

material colored
{
    u_worldViewProjectionMatrix = WORLD_VIEW_PROJECTION_MATRIX
    
    renderState
    {
        cullFace = true
        depthTest = true
    }
    
    technique
    {
        pass 
        {
            vertexShader = res/shaders/colored.vert
            fragmentShader = res/shaders/colored.frag
        }
    }
}

material lambert2 : colored
{
    u_diffuseColor = 0.4, 0.4, 0.4, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material skybox : texturedUnlit
{
    sampler u_diffuseTexture
    {
        path = res/misc/panorama_2.dds
    }

    renderState
    {
        cullFace = false
    }
}
material lambertBall1 : colored
{
    u_diffuseColor = 1, 0, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall2 : colored
{
    u_diffuseColor = 0, 1, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall3 : colored
{
    u_diffuseColor = 0, 0, 1, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall4 : colored
{
    u_diffuseColor = 1, 1, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall5 : colored
{
    u_diffuseColor = 1, 0, 1, 1 

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall6 : colored
{
    u_diffuseColor = 0, 1, 1, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall7 : colored
{
    u_diffuseColor = 1, 1, 1, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall8 : colored
{
    u_diffuseColor = 0, 0, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall9 : colored
{
    u_diffuseColor = 0.5, 0.5, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall10 : colored
{
    u_diffuseColor = 0, 0.5, 0.5, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall11 : colored
{
    u_diffuseColor = 0.3, 1, 0.3, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall12 : colored
{
    u_diffuseColor = 1, 1, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall13 : colored
{
    u_diffuseColor = 1, 1, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall14 : colored
{
    u_diffuseColor = 1, 1, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}

material lambertBall15 : colored
{
    u_diffuseColor = 1, 1, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall16 : colored
{
    u_diffuseColor = 1, 1, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall17 : colored
{
    u_diffuseColor = 1, 1, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
material lambertBall0 : colored
{
    u_diffuseColor = 1, 1, 0, 1

    u_ambientColor = SCENE_AMBIENT_COLOR
    u_directionalLightColor[0] = 1, 1, 1
    u_directionalLightDirection[0] = 0, -1, 0
    u_inverseTransposeWorldViewMatrix = INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX

    technique
    {
        pass 
        {
            defines = DIRECTIONAL_LIGHT_COUNT 1
        }
    }
}
