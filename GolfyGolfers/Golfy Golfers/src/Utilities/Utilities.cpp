#include "Utilities.h"
#include <iterator>
#include <sstream>
#include <fstream>
#include "windows.h"

#define _CRT_SECURE_NO_WARNINGS

void Utilities::PrintToOutputWindow(const char * output)
{
	// This function prints const char pointer arrays to the output window of visual studio.
	auto wn = mbsrtowcs(nullptr, &output, 0, nullptr);
	const auto buf = new wchar_t[wn + 1]();
	wn = mbsrtowcs(buf, &output, wn + 1, nullptr);
	OutputDebugStringW(buf);
	delete[] buf;
}

void Utilities::PrintToOutputWindow(const std::string* output)
{
	const auto slength = static_cast<int>(output->length()) + 1;
	const auto len = MultiByteToWideChar(CP_ACP, 0, output->c_str(), slength, nullptr, 0);
	const auto buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, output->c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	OutputDebugStringW(r.c_str());
}

void Utilities::PrintToOutputWindow(const int output)
{
	// This function prints const ints to the output window of visual studio.
	std::wstringstream wss;
	std::wstring str;
	wss << output;
	wss >> str;
	OutputDebugStringW(str.c_str());
}

void Utilities::PrintToOutputWindow(const float * output)
{
	// This function prints const floats to the output window of visual studio.
	std::wstringstream s;
	s  << &output;
	auto ws = s.str();
	OutputDebugStringW(ws.c_str());
}

float Utilities::RandomNumber(float Min, float Max)
{
	return ((float(rand()) / float(RAND_MAX)) * (Max - Min)) + Min;
}

std::vector<std::string>& Utilities::DivideString(std::string str, const char delim)
{
	// ReSharper disable once CppNonReclaimedResourceAcquisition
	auto strs = new std::vector<std::string>;
	auto pos = str.find_first_of(delim);

	while (!str.empty())
	{
		if (pos != std::string::npos)
		{
			strs->push_back(str.substr(0, pos));
			str = str.substr(pos + 1, str.size());
			pos = str.find_first_of(delim);
		}
		else
		{
			strs->push_back(str);
			str.clear();
		}
	}

	return *strs;
}

int Utilities::StringToInt(const std::string & str)
{
	// This function converts a std::string to an int.
	int result = 0;
	bool negative = false;
	std::string::const_iterator i = str.begin();

	if (i == str.end())
		return result;

	if (*i == '-')
	{
		negative = true;
		++i;

		if (i == str.end())
			return result;
	}

	for (; i != str.end(); ++i)
	{
		if (*i < '0' || *i > '9')
			return result;

		result *= 10;
		result += *i - '0';
	}

	if (negative)
	{
		result = -result;
	}

	return result;
}

std::string Utilities::FloatToString(float f)
{
	// This function converts a float to astd::string.
	return std::to_string(f);
}

float Utilities::CharToFloat(const char * c)
{
	// This function converts a character array to a float.
	return atof(c);
}

std::string Utilities::CollapseStringVector(std::vector<std::string>& vec)
{
	// This function will return 1 std::string made from a vector of std::strings.
	char result[64] = "";
	for (auto i = 0; i < vec.size() - 1; i++)
	{
		strcat_s(result, vec[i].c_str());
		strcat_s(result, ",");
	}
	strcat_s(result, vec.back().c_str());
	return result;
}

std::string Utilities::IntVectorToString(std::vector<int> vec)
{
	// This function a vector of ints to a string.
	std::stringstream result;
	std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(result, ","));
	return result.str();
}

std::vector<int> Utilities::StringToIntVector(const std::string& str)
{
	std::vector<int> ints;
	auto& strs = DivideString(str, ',');

	for (auto& str : strs)
	{
		ints.push_back(atoi(str.c_str()));
	}

	return ints;
}

std::vector<int> Utilities::StringVectorToIntVector(std::vector<std::string>& strs)
{
	std::vector<int> ints;

	for (auto& str : strs)
	{
		ints.push_back(atoi(str.c_str()));  // NOLINT
	}

	return ints;
}

std::string Utilities::Vector3ToString(gameplay::Vector3 * vec)
{
	char chars[64] = "";
	strcat_s(chars, FloatToString(vec->x).c_str());
	strcat_s(chars, ",");
	strcat_s(chars, FloatToString(vec->y).c_str());
	strcat_s(chars, ",");
	strcat_s(chars, FloatToString(vec->z).c_str());
	return std::string(chars);
}
