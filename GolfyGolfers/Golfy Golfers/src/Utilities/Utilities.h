#pragma once

#include <vector>
#include <string>
#include "Vector3.h"

class Utilities
{
public:
	static void PrintToOutputWindow(const char* output);
	static void PrintToOutputWindow(const std::string* output);
	static void PrintToOutputWindow(const int output);
	static void PrintToOutputWindow(const float* output);
	static float RandomNumber(float Min, float Max);
	static std::vector<std::string>& DivideString(std::string str, char delim);
	static int StringToInt(const std::string& str);
	static std::string FloatToString(float f);
	static float CharToFloat(const char * c);
	static std::string CollapseStringVector(std::vector<std::string>&);
	static std::string IntVectorToString(std::vector<int>);
	static std::vector<int> StringToIntVector(const std::string&);
	static std::vector<int> StringVectorToIntVector(std::vector<std::string>&);
	static std::string Vector3ToString(gameplay::Vector3* vec);	
};
