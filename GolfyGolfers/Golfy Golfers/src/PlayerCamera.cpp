#include "PlayerCamera.h"
#include "Spectate.h"
#include "Managers/GameManager.h"

Spectate* spectate = new Spectate();

PlayerCamera::PlayerCamera(Node* pCameraNode, Node* pBallNode, PlayerClass* pPlayerClass)
{
	cameraNode = pCameraNode;
	ballNode = pBallNode;
	playerClass = pPlayerClass;
	vectorToTarget = ballNode->getBackVector();

	rotateKey = Keyboard::KEY_CTRL;
	isHoldingRotateKey = false;
}


PlayerCamera::~PlayerCamera()
{
}

void PlayerCamera::update(float elapsedTime) 
{
	target = ballNode->getTranslation();
	Vector3 watchFromPosition = target + Vector3::unitY()*4.0f + Vector3(vectorToTarget.x, 0, vectorToTarget.z) * cameraDistance;
	setPosition(watchFromPosition);
	lookAtTarget(ballNode->getTranslation());
}

void PlayerCamera::onTouchInput(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex)
{
	switch (evt)
	{
	case Touch::TOUCH_PRESS:
		isDragging = true;
		break;
	case Touch::TOUCH_RELEASE:
		isDragging = false;
		dragDistance = 0;

		break;
	case Touch::TOUCH_MOVE:
		if (!isHoldingRotateKey) break;
		rotateAroundY((x - oldDragPosition.x)/100);
		oldDragPosition = Vector3(x, y, 0);
		break;
	}
}
void PlayerCamera::onKeyboardInput(Keyboard::KeyEvent evt, int key) {
	if (evt == Keyboard::KEY_PRESS)
	{
		switch (key)
		{
		case Keyboard::KEY_Z:
			rotateAroundY(0.05f);
			update(0);
			break;
		case Keyboard::KEY_R:
			rotateAroundY(-0.05f);
			update(0);
			break;
		case Keyboard::KEY_I:
			zoomCamera(-0.05);
			break;
		case Keyboard::KEY_O:
			zoomCamera(0.05);
			break;
		case Keyboard::KEY_T:
			lookAtTarget(Vector3(10, 1000, 0));
			break;
		case Keyboard::KEY_L:
			spectate->SpectateNext();
			break;
		}
		if (key == rotateKey)
		{
			isHoldingRotateKey = true;
		}
	}
	else if (evt == Keyboard::KEY_RELEASE)
	{
		if (key == rotateKey)
		{
			isHoldingRotateKey = false;
		}
	}
}

void PlayerCamera::zoomCamera(float amount)
{
	vectorToTarget = vectorToTarget * (1 + amount);
}
/**
	Rotate the camera by a set angle around its target.

	@param angle Angle in which the rotation is done
*/
void PlayerCamera::rotateAroundY(float angle) 
{
	vectorToTarget = Vector3(vectorToTarget.x*cos(angle) + vectorToTarget.z * sin(angle),
		vectorToTarget.y,
		-vectorToTarget.x * sin(angle) + vectorToTarget.z * cos(angle));
}
/**
	Translates the position of the camera to given target.

	@param pTarget The new target postion in which the camera should translate to

*/
void PlayerCamera::setPosition(Vector3 pTarget) {

	cameraNode->translateSmooth(pTarget, 1, 1);
}

/**
	Rotates the camera towards the target.

	@param pTarget The target the camera should rotate towrds
*/
void PlayerCamera::lookAtTarget(Vector3 pTarget) {
	// Get the matrix from current translation towards target vector
	Matrix m;
	Matrix::createLookAt(cameraNode->getTranslation(), pTarget, Vector3::unitY(), &m); 
	m.transpose();
	// Put the new matrix into the new rotation quaternion
	Quaternion q;
	m.getRotation(&q);
	cameraNode->setRotation(q);
}

Node* PlayerCamera::getCameraNode()
{
	return cameraNode;
}