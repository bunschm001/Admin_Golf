#ifndef GolfyGolfersMain_H_
#define GolfyGolfersMain_H_

#include <thread>
#include "gameplay.h"
#include "Misc/PlayerCamera.h"
#include "Misc/PlayerClass.h"
#include "Utilities/Utilities.h"
#include "Managers/GameManager.h"
#include "Networking/Network.h"
#include "Particle System/ParticleSystem.h"
#include "Managers/SceneManager.h"
using namespace gameplay;

/**
* Main game class.
*/
class GolfyGolfersMain : public Game, Control::Listener
{
public:
	float power = 0;
	bool pressed = false;
	bool showScoreMenu;
	btVector3 start, end;
	Vector3 forwardvector;
	unsigned int distance;
	Font* font;
	Node* ballNode;
	int rotateX, deltaX;
	ParticleSystem* testParticleSystem;
	bool mouseEvent(Mouse::MouseEvent evt, int x, int y, int wheelDelta) override;
	/**
	* Constructor.
	*/
	GolfyGolfersMain();

	/**
	* @see Game::keyEvent
	*/
	void keyEvent(Keyboard::KeyEvent evt, int key) override;

	/**
	* @see Game::touchEvent
	*/
	void touchEvent(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex) override;

	/**
	* @see Control::Listener::controlEvent
	*/
	void controlEvent(Control* control, EventType evt) override;

protected:

	/**
	* @see Game::initialize
	*/
	void initialize() override;

	/**
	* @see Game::finalize
	*/
	void finalize() override;

	/**
	* @see Game::update
	*/
	void update(float elapsedTime) override;

	/**
	* @see Game::render
	*/
	void render(float elapsedTime) override;

	/**
	* Load first level from lobby.
	*/
	void LeaveLobby();

private:
	/**
	* Draws the scene each frame.
	*/
	bool DrawScene(Node* node);
	const char * CreateScoreMenu(std::vector<Opponents*>* opponents) const;
	const char * CreateLobbyMenu(std::vector<Opponents*>* opponents) const;

	bool Wireframe_;
	Form* Lobby_;
	Form* Menu_;
	Form* inGameMenu;
	Form* inGameUI;
	Form* inGameUIPower;
	//PlayerCamera* _playerCamera;
	//Node* _golfBall;
};

#endif