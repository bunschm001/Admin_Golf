#include "SandHandler.h"
#include "../Managers/GameManager.h"
//#include "golfBall.h"
#include <iostream>
#define SAND_SLOW_DOWN 0.95f


/**
* \brief This class handles the velocity of the ball when golfball collides with sand.
* \param pBallNode
* \param pSandNode
*/
SandHandler::SandHandler()
{
}
void SandHandler::Update(float elapsedTime)
{
	if (isSand == true) {
		const auto instance = GameManager::GetInstance();
		Vector3 velocity = static_cast<PhysicsRigidBody*>(instance->player->GetGolfBall()->GetNode()->getCollisionObject())->getLinearVelocity();
		velocity *= SAND_SLOW_DOWN;
		static_cast<PhysicsRigidBody*>(instance->player->GetGolfBall()->GetNode()->getCollisionObject())->setLinearVelocity(velocity);
	}
}

void SandHandler::collisionEvent(PhysicsRigidBody::CollisionListener::EventType type,
	const PhysicsRigidBody::CollisionPair& pair,
	const Vector3& pointA, const Vector3& pointB)
{
	isSand = !isSand;
}

void SandHandler::CheckColBetween(Node* ball, Node* sand)
{
	sand->getCollisionObject()->addCollisionListener(this, ball->getCollisionObject());
}