#pragma once
#ifndef PUTTHANDLER_H
#define PUTTHANDLER_H

#include "gameplay.h"
#include "../Particle System/ParticleSystem.h"
#include "../Managers/AudioManager.h"
using namespace gameplay;

class PuttHandler : public gameplay::PhysicsCollisionObject::CollisionListener
{
public:
	PuttHandler();
	void Update(float elapsedTime);
	std::time_t inPut = -1;
	void collisionEvent(PhysicsRigidBody::CollisionListener::EventType type,
		const PhysicsRigidBody::CollisionPair& pair,
		const Vector3& pointA, const Vector3& pointB) override;

	void SpectateAfterPeriod(int seconds);
	void FinishAfterPeriod(int seconds);
	void CheckColBetween(Node* ball, Node* putt);
	ParticleSystem* putParticleSystem;
};
#endif