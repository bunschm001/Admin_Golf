#ifndef PLAYER_CLASS_H
#define PLAYER_CLASS_H
#include "PlayerCamera.h"
#include "golfBall.h"

using namespace gameplay;

class PlayerClass
{
	PlayerClass* PlayerReference_;
	GolfBall* GolfBall_;
	PlayerCamera* PlayerCamera_;
	

public:
	PlayerClass(Node* pCameraNode, Node* pBallNode);

	bool putted = false;

	GolfBall* GetGolfBall() const;
	PlayerCamera* GetCamera() const;
	void Update(float elapsedTime) const;
	void OnKeyboardInput(Keyboard::KeyEvent evt, int key) const;
	void OnTouchInput(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex) const;
	void OnMouseEvent(const Mouse::MouseEvent evt, int x, int y, int wheelDelta) const;
};

#endif //PLAYER_CLASS_H

