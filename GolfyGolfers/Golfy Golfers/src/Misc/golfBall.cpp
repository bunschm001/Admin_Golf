#include "../Managers/GameManager.h"
#include "golfBall.h"
#include "PlayerClass.h"

#define PHYSICS_COLLISION_GROUP_DEFAULT btBroadphaseProxy::
#define PHYSICS_COLLISION_MASK_DEFAULT btBroadphaseProxy::AllFilter

/**
 * \brief This class handles the physics of the ball object. It is instantiated by the playerClass.  
 * \param pBallNode 
 * \param pPlayerClass 
 */
GolfBall::GolfBall(Node* pBallNode, PlayerClass* pPlayerClass)
{
	int id = GameManager::GetInstance()->ownId;
	BallNode_ = pBallNode;
	PlayerClass_ = pPlayerClass;
	BallModel_ = dynamic_cast<Model*>(BallNode_->getDrawable());
	char path[128] = "";
	strcat(path, "res/demo.material#lambertBall");
	strcat(path, std::to_string(id).c_str());
	dynamic_cast<Model*>(BallNode_->getDrawable())->setMaterial(path, 0);


	BallNode_->setTranslation(GameManager::GetInstance()->startPos);
	PhysicsRigidBody::Parameters rbParams(1, 1, 1, 0.5f, 0.5f);
	BallNode_->setCollisionObject(PhysicsCollisionObject::RIGID_BODY, PhysicsCollisionShape::sphere(0.5f), &rbParams, -1, 1);
	PhysicsObject_ = dynamic_cast<PhysicsRigidBody*>(BallNode_->getCollisionObject());
	GameManager::GetInstance()->scene->addNode(BallNode_);
	BallNode_->release();


	DragDistance_ = 0;
	Power_ = 1;
	IsApplyingPower_ = false;
	isRolling = false;
}

void GolfBall::Update(float elapsedtime)
{
	if(isRolling)
	{
		if(PhysicsObject_->getLinearVelocity().length() < BALLSTOPSPEED)
		{
			isRolling = false;
			PhysicsObject_->setLinearVelocity(0, 0, 0);
			GameManager::GetInstance()->mNetwork->SendInstructionToServer(networking::UPDATE_POSITION, Utilities::Vector3ToString(&(gameplay::Vector3)BallNode_->getTranslation()).c_str());
		}
	}

	if (BallNode_->getTranslationY() <= RESETDEPTH)
	{
		ResetToLastPosition();
	}

	GameManager::GetInstance()->ballPower = DragDistance_;

}
/*
	This method handles the keyboard input for the golfball. It is called in the main class.
*/
void GolfBall::OnTouchInput(const Touch::TouchEvent evt, int x, int y, unsigned int contactIndex)
{
	switch (evt)
	{
	case Touch::TOUCH_PRESS:
		if (!isRolling)
		{
			StartDragPosition_ = Vector3(x, y, 0);
			IsApplyingPower_ = true;

			
		}
		
		// TODO Lock camera rotation
		break;
	case Touch::TOUCH_RELEASE:
	{
		if (!isRolling)
		{
			auto instance = GameManager::GetInstance();
			if (!IsApplyingPower_) break;

			IsApplyingPower_ = false;
			if (DragDistance_ <= 0)
			{
				DragDistance_ = 0;
				break;
			}

			// Calculate forward vector
			ForwardVector_ = PhysicsObject_->getNode()->getTranslation() - PlayerClass_->GetCamera()->GetCameraNode()->getTranslation();
			ForwardVector_.y = 0;
			ForwardVector_.normalize();

			LastPosition_ = BallNode_->getTranslation();
			auto impulseVector = ForwardVector_ * (0.02f * pow(DragDistance_, 1.5f));
			PhysicsObject_->applyImpulse(impulseVector);
			AudioManager::GetInstance()->ballHitSound->play();
			instance->mNetwork->SendInstructionToServer(networking::UPDATE_FORCE, Utilities::Vector3ToString(&impulseVector).c_str());
			instance->score++;
			if (instance->isMultiplayer)
				instance->SaveScore();
			else
				instance->singlePlayerScores[instance->level] = instance->score;
			isRolling = true;
			DragDistance_ = 0;
		}
		
		// TODO Do Network callback
		break;
	}
	case Touch::TOUCH_MOVE:
		if (!isRolling)
		{
			if (!IsApplyingPower_) break;
			DragDistance_ = std::abs(StartDragPosition_.y - y);
			if (DragDistance_ >= MAXPOWER) DragDistance_ = MAXPOWER;
			
		}		
		break;
	};
}

/**
* \brief This function resets the balls position to the levels start position.
*/
void GolfBall::ResetToStartPosition()
{
	PhysicsObject_->setEnabled(false);
	BallNode_->setTranslation(GameManager::GetInstance()->startPos);
	PhysicsObject_->setEnabled(true);
	PhysicsObject_->setLinearVelocity(0, 0, 0);
	PhysicsObject_->setAngularVelocity(0, 0, 0);
}

/**
* \brief This function resets the balls position to its position before its last impulse.
*/
void GolfBall::ResetToLastPosition()
{
	PhysicsObject_->setEnabled(false);
	BallNode_->setTranslation(LastPosition_);
	PhysicsObject_->setEnabled(true);
	PhysicsObject_->setLinearVelocity(0, 0, 0);
	PhysicsObject_->setAngularVelocity(0, 0, 0);
}
