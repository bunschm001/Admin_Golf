#include "Spectate.h"
#include "../Managers/GameManager.h"

Spectate* Spectate::Instance_;

/**
 * Constructor of the Spectate singleton
 */
Spectate::Spectate()
{
	if (Instance_ == nullptr)
	{
		//There is no isntance yet, while i'm being constructed. I will become the instance.
		Instance_ = this;
	}else
	{
		//There already is an instance, while i'm being constructed. I won't be instanciated.
		delete this;
	}
}
	
/**
 *Sets the camera target to the next ball node
 */
void Spectate::SpectateNext()
{
	//Get the instance of the GameManager, this will be used later in the method
	auto instance = GameManager::GetInstance();

	//If the instance is present/not null
	if (instance) {

		//Set the default loopCounter to -1.
		auto loopCounter = -1;

		//check whether MyIndex is -1 (default value for MyIndex is -1, header). 
		//Since deafult value is -1 this will always result in true at least the first time since launch of the application
		if(myIndex == -1)
		{
			//Start a for loop starting from 0 (i) to the size of the array.
			for (auto i = 0; i < instance->opponents.size(); i++)
			{
				//if the index from the array which we loop trough is not a nullptr
				if (instance->opponents[i] != nullptr)
				{
					//if the index at the array has an id that matches my own id.
					if (instance->opponents[i]->id == instance->ownId)
					{
						//if the index at the array is NOT playing.
						if(!instance->opponents[i]->isPlaying)
						{
							//Set MyIndex to the current index.
							//This is our index in the array which should not change
							myIndex = i;

							//Set the current loopCounter to MyIndex.
							loopCounter = myIndex;

							//Break the for-loop since we found our only index we are after, no point in continuing
							break;
						}
					}
				}
			}
		}
		//We are already spactating someone, else myIndex would be -1
		else
		{
			//So loopCounter is the spectatorindex
			loopCounter = indexSpectator;
		}

		if (myIndex == -1)
		{
			//Could not find my own index!
			return;
		}

		for (auto j = 0; j < instance->opponents.size(); j++)
		{
			const auto indexCheck = loopCounter + j;
			if (instance->opponents[indexCheck] != nullptr)
			{
				if (instance->opponents[indexCheck]->isPlaying && indexCheck != indexSpectator)
				{
					indexSpectator = indexCheck;
					loopCounter = indexCheck;
					break;
				}
			}

			if (indexCheck >= (instance->opponents.size()-1))
			{
				loopCounter = -(j + 1);
			}
		}

		if (indexSpectator == -1)
		{
			//Never found next player
			return;
		}
		if (instance->opponents[indexSpectator]->ballNode == nullptr) {
			//Can't find node on next target!
			throw std::exception("Next target doesn't have a node");
			//return;
		}
		else {
			//found next node to spectate!
			instance->playerCam->ballNode = instance->opponents[indexSpectator]->ballNode;
		}
	}
}
