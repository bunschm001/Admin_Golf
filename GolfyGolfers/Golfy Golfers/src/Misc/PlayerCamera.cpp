#include "PlayerCamera.h"
#include "Spectate.h"
#include "../Managers/GameManager.h"

PlayerCamera::PlayerCamera(Node* pCameraNode, Node* pBallNode, PlayerClass* pPlayerClass): RotationOffsetInDegrees_(0),
                                                                                           IsDragging_(false),
                                                                                           DragDistance_(0)
{
	minCameraHeight = 0.0f;
	maxCameraHeight = 2.0f;
	cameraHeight = 0.5f;
	CameraNode_ = pCameraNode;
	ballNode = pBallNode;
	PlayerClass_ = pPlayerClass;
	VectorToTarget_ = ballNode->getBackVector();
	RotateKey_ = Keyboard::KEY_CTRL;
	IsHoldingRotateKey_ = false;
	rotationSpeed = 2;
	zoomPower = 2.0f;
	currentCameraDistance = 20;
	minCameraDistance = 5;
	maxCameraDistance = 220;

	
	//ShowCursor(false);
	// Clips mouse to window
#pragma region clip window
	HWND mWindow = GetActiveWindow();
	RECT rect;
	GetClientRect(mWindow, &rect);

	POINT ul;
	ul.x = rect.left;
	ul.y = rect.top;

	POINT lr;
	lr.x = rect.right;
	lr.y = rect.bottom;

	MapWindowPoints(mWindow, nullptr, &ul, 1);
	MapWindowPoints(mWindow, nullptr, &lr, 1);

	rect.left = ul.x;
	rect.top = ul.y;

	rect.right = lr.x;
	rect.bottom = lr.y;

	ClipCursor(&rect);
#pragma endregion clip window
}


PlayerCamera::~PlayerCamera()
= default;

void PlayerCamera::Update(float elapsedTime) 
{
	if (!GameManager::GetInstance()->onInGameMenu)
	{
		Target_ = ballNode->getTranslation();
		VectorToTarget_.normalize();
		const auto watchFromPosition = (Target_ + ((VectorToTarget_ + Vector3(0, cameraHeight, 0)) * currentCameraDistance));
		SetPosition(watchFromPosition);
		LookAtTarget(ballNode->getTranslation());
	}	
}

void PlayerCamera::OnTouchInput(const Touch::TouchEvent evt, const int x, const int y, unsigned int contactIndex)
{
	switch (evt)
	{
	case Touch::TOUCH_PRESS:
		IsDragging_ = true;
		break;
	case Touch::TOUCH_RELEASE:
		IsDragging_ = false;
		DragDistance_ = 0;
		break;
	}
}

void PlayerCamera::OnMouseEvent(const Mouse::MouseEvent evt, int x, int y, int wheelDelta)
{
	switch (evt)
	{
	case Mouse::MOUSE_MOVE:
		RotateAroundY(-(x - OldDragPosition_.x) / 1200);
		if(!IsDragging_)AddToCameraHeight((y - OldDragPosition_.y) / 1200);
		RECT rect;
		OldDragPosition_ = Vector3(x, y, 0);
		if (GetWindowRect(GetActiveWindow(), &rect))
		{ // WARNING: Only working in fullscreen mode
			// TODO: Fix it where this would work in windowed mode aswell
			int width = rect.right - rect.left;
			int height = rect.bottom - rect.top;
			if (x <= 5 || x >= width - 25)
			{
				int newX = rect.left + width/2;
				SetCursorPos(newX, y);
				OldDragPosition_ = Vector3(newX, y, 0);
			} 
		}
		break;
	case Mouse::MOUSE_WHEEL:

		if (wheelDelta > 0)
		{
			ZoomCamera(-zoomPower);
		}
		else if (wheelDelta < 0)
		{
			ZoomCamera(zoomPower);
		}

		break;
	}
}

void PlayerCamera::OnKeyboardInput(const Keyboard::KeyEvent evt, const int key) {
	if (evt == Keyboard::KEY_PRESS)
	{
		switch (key)
		{
		case Keyboard::KEY_Z:
			RotateAroundY(0.05f);
			break;
		case Keyboard::KEY_X:
			RotateAroundY(-0.05f);
			break;
		case Keyboard::KEY_Q:
			AddToCameraHeight(0.05f);
			break;
		case Keyboard::KEY_A:
			AddToCameraHeight(-0.05f);
			break;
		case Keyboard::KEY_I:
			ZoomCamera(-zoomPower);
			break;
		case Keyboard::KEY_O:
			ZoomCamera(zoomPower);
			break;
		case Keyboard::KEY_T:
			LookAtTarget(Vector3(10, 1000, 0));
			break;
		case Keyboard::KEY_L:
			Spectate::GetInstance()->SpectateNext();
			break;
		default: 
			break;
		}
		if (key == RotateKey_)
		{
			IsHoldingRotateKey_ = true;
		}
	}
	else if (evt == Keyboard::KEY_RELEASE)
	{
		if (key == RotateKey_)
		{
			IsHoldingRotateKey_ = false;
		}
	}
}


/**
 * \brief Zooms the camera in by a x amount based on parameter.
 * \param amount 
 */
void PlayerCamera::ZoomCamera(const float amount)
{
	// TODO: Change hard coded distance to configurable values
	if (currentCameraDistance >= minCameraDistance && currentCameraDistance <= maxCameraDistance)
	{
		currentCameraDistance += amount;
		if (currentCameraDistance > maxCameraDistance) currentCameraDistance = maxCameraDistance;
		else if (currentCameraDistance < minCameraDistance) currentCameraDistance = minCameraDistance;
		
	}
}

/**
	Rotate the camera by a set angle around its target.

	@param angle Angle in which the rotation is done
*/
void PlayerCamera::RotateAroundY(const float angle) 
{
	Matrix m;
	Matrix::createRotationY(angle, &m);
	m.rotateY(angle);
	Quaternion q;
	m.getRotation(&q);
	q.rotatePoint(VectorToTarget_, &VectorToTarget_);
}

/**
 * \brief Adds a x amount to the cameraHeight based on parameter
 * \param amount 
 */
void PlayerCamera::AddToCameraHeight(float amount)
{
	cameraHeight += amount;
	// Clamp function
	cameraHeight = cameraHeight < minCameraHeight ? minCameraHeight : cameraHeight > maxCameraHeight ? maxCameraHeight : cameraHeight;
}


/**
	Translates the position of the camera to given target.

	@param pTarget The new target postion in which the camera should translate to

*/
void PlayerCamera::SetPosition(const Vector3 pTarget) const
{
	CameraNode_->setTranslation(pTarget);
}

/**
	Rotates the camera towards the target.

	@param pTarget The target the camera should rotate towrds
*/
void PlayerCamera::LookAtTarget(const Vector3 pTarget) const
{
	// Get the matrix from current translation towards target vector
	Matrix m;
	Matrix::createLookAt(CameraNode_->getTranslation(), pTarget, Vector3::unitY(), &m); 
	m.transpose();
	// Put the new matrix into the new rotation quaternion
	Quaternion q;
	m.getRotation(&q);
	CameraNode_->setRotation(q);
}

Node* PlayerCamera::GetCameraNode() const
{
	return CameraNode_;
}
