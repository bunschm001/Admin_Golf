#ifndef PlayerCamera_H_
#define PlayerCamera_H_
#include "gameplay.h"

using namespace gameplay;
/**
	This is the class for the camera that the players sees through.

	It handles the functions to follow the ball which the player controlls, it alows for rotation around an object.
	It's main component comes from the Camera node which Gamepay3d provides, see "camera.h" for indepth functions.
	The camera is decoupled from the golf ball because it alows for free movement and lock on on several other targets.
**/

class PlayerClass;
class PlayerCamera
{	
	PlayerClass* PlayerClass_;
	Node* CameraNode_;
	Vector3 Target_;

	Vector3 VectorToTarget_;
	float RotationOffsetInDegrees_;
	bool IsDragging_;
	Vector3 OldDragPosition_;
	float DragDistance_;

	int RotateKey_;
	bool IsHoldingRotateKey_;

public:
	Node* ballNode;
	PlayerCamera(Node* pCameraNode, Node* pBallNode, PlayerClass* pPlayerClass);
	~PlayerCamera();
	void Update(float elapsedTime);
	void OnKeyboardInput(Keyboard::KeyEvent evt, int key);
	void OnTouchInput(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex);
	void OnMouseEvent(const Mouse::MouseEvent evt, int x, int y, int wheelDelta);
	void ZoomCamera(float amount);
	void SetPosition(Vector3 pTarget) const;
	void RotateAroundY(float angle);
	void AddToCameraHeight(float amount);
	void LookAtTarget(Vector3 pTarget) const;

	Node* GetCameraNode() const;
private:
	Vector3 Position_;
	Matrix currentRotationMatrix;
	float cameraHeight;
	float minCameraHeight, maxCameraHeight;
	float rotationSpeed;
	float currentCameraDistance;
	float minCameraDistance;
	float maxCameraDistance;
	float zoomPower;
};
#endif
