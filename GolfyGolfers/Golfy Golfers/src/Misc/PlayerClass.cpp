#include "PlayerClass.h"


/**
 * \brief This class handles the golfBall and the playerCamera It is used to keep the player classes in a central class. 
 * It handles the update and input of the player classes.
 * \param pCameraNode Camera of the player (client)
 * \param pBallNode  The ball of the player (client)
 */
PlayerClass::PlayerClass(Node* pCameraNode, Node* pBallNode)
{
	PlayerReference_ = this;
	PlayerCamera_ = new PlayerCamera(pCameraNode, pBallNode, PlayerReference_);
	GolfBall_ = new GolfBall(pBallNode, PlayerReference_);
}

/**
 * \brief Will call update for the golfball and camera.
 * \param elapsedTime Time from last frame till current frame in milliseconds
 */
void PlayerClass::Update(const float elapsedTime) const
{
	GolfBall_->Update(elapsedTime);
	PlayerCamera_->Update(elapsedTime);
}

/**
 * \brief Handles keyboard input.
 * \param evt
 * \param key 
 */
void PlayerClass::OnKeyboardInput(const Keyboard::KeyEvent evt, const int key) const
{
	PlayerCamera_->OnKeyboardInput(evt, key);
}

/**
 * \brief Handles touch input
 * \param evt 
 * \param x 
 * \param y 
 * \param contactIndex 
 */
void PlayerClass::OnTouchInput(const Touch::TouchEvent evt, const int x, const int y, const unsigned int contactIndex) const
{
	GolfBall_->OnTouchInput(evt, x, y, contactIndex);
	PlayerCamera_->OnTouchInput(evt, x, y, contactIndex);
}

void PlayerClass::OnMouseEvent(const Mouse::MouseEvent evt, int x, int y, int wheelDelta) const
{
	PlayerCamera_->OnMouseEvent(evt, x, y, wheelDelta);
}

/**
 * \brief Returns the golfBall.
 * \return 
 */
GolfBall* PlayerClass::GetGolfBall() const
{
	return GolfBall_;
}

/**
 * \brief Return the playerCamera.
 * \return 
 */
PlayerCamera* PlayerClass::GetCamera() const
{
	return PlayerCamera_;
}
