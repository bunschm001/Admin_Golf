#ifndef SPECTATE_H
#define SPECTATE_H

#include "gameplay.h"

using namespace gameplay;

class Spectate {

public:
	static Spectate* GetInstance()
	{
		if (!Instance_)
			Instance_ = new Spectate();
		return Instance_;
	}
	Spectate();

	void SpectateNext();
	int specIndex;
	int myIndex = -1;
	int indexSpectator = -1;

private:
	static Spectate* Instance_;
};
#endif // Spectate

