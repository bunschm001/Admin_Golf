#pragma once
#ifndef SANDHANDLER_H
#define SANDHANDLER_H

#include "gameplay.h"


using namespace gameplay;

class SandHandler : public gameplay::PhysicsCollisionObject::CollisionListener
{
	Node* ballNode;
	Node* sandNode;

public:

	SandHandler();
	void Update(float elapsedTime);
	bool isSand = false;
	int teller;

	void collisionEvent(PhysicsRigidBody::CollisionListener::EventType type,
		const PhysicsRigidBody::CollisionPair& pair,
		const Vector3& pointA, const Vector3& pointB);
	void CheckColBetween(Node* ball, Node* sand);

};
#endif