#pragma once
#ifndef GOLFBALL_H
#define GOLFBALL_H

#define RESETDEPTH -5
#define MAXPOWER 468

#include "gameplay.h"
#include "../Managers/AudioManager.h"

using namespace gameplay;

class PlayerClass;

class GolfBall
{
	PlayerClass* PlayerClass_;
	Node* BallNode_;
	PhysicsRigidBody* PhysicsObject_;
	Model* BallModel_;
	//Material BallMaterial_;

	Vector3 ForwardVector_;

	bool IsApplyingPower_;
	Vector3 StartDragPosition_;
	unsigned int DragDistance_;
	float Power_;
	float BallStopSpeed_;
public:
	bool isRolling;

	GolfBall(Node* pBallNode, PlayerClass* pPlayerClass);
	~GolfBall();
	void Update(float elapsedTime);
	void OnTouchInput(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex);
	void ResetToStartPosition();
	void ResetToLastPosition();
	Node* GetNode() const { return BallNode_; }

private:
	Vector3 LastPosition_;
};

#endif

