#include "puttHandler.h"
#include "Spectate.h"
#include "../Managers/GameManager.h"

/**
* \brief This class handles the deletion and pass through of points when golfball in putt.
*/
PuttHandler::PuttHandler()
{
}

bool isSpectating = false;

/**
 * Calls the SpactateAfterPeriod function so that we have automatic spectating
 */
void PuttHandler::Update(float elapsedTime)
{
	SpectateAfterPeriod(3);
}

/**
 * Checks if collision between the two given objects in CheckColBetween is happening
 * 
 */
void PuttHandler::collisionEvent(PhysicsRigidBody::CollisionListener::EventType type,
	const PhysicsRigidBody::CollisionPair& pair,
	const Vector3& pointA, const Vector3& pointB)
{
	const auto ball = pair.objectA->getNode();
	const auto instance = GameManager::GetInstance();
	putParticleSystem = new ParticleSystem(instance->particleSystems.size(), false, 125, 100, false, 2500, instance->scene->findNode("hole"));
	
	// Send score & state
	if (instance->isMultiplayer)
	{
		instance->SaveScore();
		if (ball == instance->player->GetGolfBall()->GetNode() && !instance->player->putted)
		{
			instance->AddParticleSystem(putParticleSystem);
			putParticleSystem->Emit();
			instance->player->putted = true;
			inPut = std::time(nullptr);
		}
	}
	else
	{
		if (ball == instance->player->GetGolfBall()->GetNode() && !instance->player->putted)
		{
			instance->AddParticleSystem(putParticleSystem);
			putParticleSystem->Emit();
			instance->player->putted = true;
			inPut = std::time(nullptr);
		}
	}
}

/**
 * Sets the camera target to the next ball node after given seconds
 * 
 *@params seconds are used to define the period duration
 *
 */
void PuttHandler::SpectateAfterPeriod(int seconds)
{
	if (inPut != -1)
	{
		if ((time(nullptr) - inPut) > 3.0f)
		{
			try
			{
				if (GameManager::GetInstance()->isMultiplayer)
				{
					GameManager::GetInstance()->SaveState(false);
				}
				else
				{
					if (GameManager::GetInstance()->level < 2)
					{
						GameManager::GetInstance()->level++;
						GameManager::GetInstance()->isLoadingNextLevel = true;
					}
					else
					{
						GameManager::GetInstance()->isLoadingMenu = true;
					}
				}
				Spectate::GetInstance()->SpectateNext();
				inPut = -1;
			}
			catch(std::exception e)
			{
				Utilities::PrintToOutputWindow(e.what());
			}
		}
	}
}

/**
 *Sets a collision listener between ball and putt,
 *This is used in the CollisionEvent function to see if there is collision
 *
 *@params ball is the golfball node you are playing, putt is the hole node of the map.
 *
 */
void PuttHandler::CheckColBetween(Node* ball, Node* putt)
{
	putt->getCollisionObject()->addCollisionListener(this, ball->getCollisionObject());
}

