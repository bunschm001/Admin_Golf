#pragma once
#ifndef PARTICLE_SYSTEM_H
#define PARTICLE_SYSTEM_H
#include <gameplay.h>
#include <random>
#include "Particle.h"
#include "../Utilities/Utilities.h"

using namespace gameplay;
class GameManager;

class ParticleSystem
{
public:public:
	bool operator == (const ParticleSystem& p) const { return id == p.id; }
	bool operator != (const ParticleSystem& p) const { return !operator==(p); }
	ParticleSystem(int id, bool pEmitOnce, float pEmitInterval, int pParticlesPerEmit, bool autoStart, float particleLifeTime, const Node* pTargetNode);
	int id;
	void Update(float elapsedTime);
	void Draw() const;
	bool isAlive() const;
	bool canEmit() const;
	void SetTexture(char* texture); // TODO: Add blendnode.
	void Emit();
	void Start();
	void Stop();
	Particle* particles;
	int particleCount;
	Node* particleSystemNode;
	const Node* targetNode;
	SpriteBatch* spriteBatch;

	bool isRunning{};
	bool emitOnce;
	float emitInterval;
	int particlesPerEmit;
	float particleLifeTime;
private:
	void EmitParticles(int amount);
	float timeToEmit;
	bool isEmitting;
};
#endif
