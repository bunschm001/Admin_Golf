#include "ParticleSystem.h"
#include "../Managers/GameManager.h"

#define MAX_PARTICLES                       100000

/**
 * \brief WIP This is the particle system class. After it has been made the texture can be changed. It is seperate of the GamePlay3d particle emitter
 *				because we need more freedom in the particles itself. This will help us creating particles systems faster in our own in the future.
 * \param pEmitOnce WIP: Will the particle system emit a particle batch once or will it loop
 * \param pEmitInterval 
 * \param pParticlesPerEmit The amount of particles that will be emitted per emit
 * \param autoStart Will it start on creation
 * \param pParticleLifetime How long the particles will live.
 * \param ptargetNode The node which this particle system will be added to
 */
ParticleSystem::ParticleSystem(int id,  bool pEmitOnce = false, float pEmitInterval = 500, const int pParticlesPerEmit = 15, const bool autoStart = false, const float pParticleLifetime = 5000, const Node* ptargetNode = nullptr) :
id(id), emitOnce(pEmitOnce), emitInterval(pEmitInterval), particlesPerEmit(pParticlesPerEmit), particleLifeTime(pParticleLifetime), isEmitting(autoStart), targetNode(ptargetNode)
{
	std::srand(std::time(0));
	spriteBatch = nullptr;
	particleCount = 0;
	// The particles controlled by this particle system. Will be used for Object Pool (See: http://gameprogrammingpatterns.com/object-pool.html)
	// This is because we dont want to create and destroy particles frequently
	particles = new Particle[MAX_PARTICLES]; 
	timeToEmit = 0;
	particleSystemNode = GameManager::GetInstance()->scene->addNode();
	SetTexture("src/Images/Particles/particle-basic-round.png");
	// TODO: switch next line to target refernce position
	if(targetNode) particleSystemNode->setTranslation(targetNode->getTranslation());
}

/**
 * \brief Will update the particle system and the particles within.
 * \param elapsedTime Time from last frame till current frame in milliseconds
 */
void ParticleSystem::Update(float elapsedTime)
{
	if (!targetNode) {
		GameManager::GetInstance()->RemoveParticleSystem(this);
		return;
	}

	if (isEmitting) {
		timeToEmit += elapsedTime;
		if (timeToEmit >= emitInterval)
		{
			EmitParticles(particlesPerEmit);
			timeToEmit = 0;
		}
	}

	if (!isAlive()) return;

	if (targetNode) particleSystemNode->setTranslation(targetNode->getTranslation());

	for (unsigned int i = 0; i < particleCount; i++)
	{
		Particle* p = &particles[i];

		if(!p->Update(elapsedTime))
		{
			if (i != particleCount - 1)
			{
				particles[i] = particles[particleCount - 1];
			}
			--particleCount;
		};
	}
}

/**
 * \brief WIP Draws all the particles that are alive on its correct position and scale.
 */
void ParticleSystem::Draw() const
{
	if (!targetNode) return;
	if (!isAlive()) return;
	spriteBatch->start();

	// TODO Fix problem where scene switches
	spriteBatch->setProjectionMatrix(particleSystemNode->getViewProjectionMatrix());
	static const Vector2 pivot(0.5f, 0.5f);

	//Get matrix so that the particle will face towards the camera
	const Matrix& cameraWorldMatrix = GameManager::GetInstance()->scene->getActiveCamera()->getNode()->getWorldMatrix();
	Vector3 right;
	cameraWorldMatrix.getRightVector(&right);
	Vector3 up;
	cameraWorldMatrix.getUpVector(&up);

	for (unsigned int i = 0; i < particleCount; i++)
	{
		Particle* p = &particles[i];
		p->Position();
		spriteBatch->draw(p->position, right, up, 0.2f, 0.2f,
			0, 0, 1, 1,
			p->color, pivot, p->angle);
	}
	spriteBatch->finish();
}

/**
 * \brief Checks if this particle system is alive.
 * \return 
 */
bool ParticleSystem::isAlive() const
{
	return (particleCount > 0);
}

/**
 * \brief Will emit a batch of particles based on particlesPerEmit.
 */
void ParticleSystem::Emit()
{
	EmitParticles(particlesPerEmit);
}

/**
 * \brief Will start the emitting of the particles.
 */
void ParticleSystem::Start()
{
	isEmitting = true;
}

/**
 * \brief Stops emitting of the particles.
 */
void ParticleSystem::Stop()
{
	isEmitting = false;
}


// ReSharper disable CppUseAuto
/**
 * \brief Sets the texture the particle will use.
 * \param texture Texture to set.
 */
void ParticleSystem::SetTexture(char* texture)
{
	SpriteBatch* batch = SpriteBatch::create(texture);

	SAFE_DELETE(spriteBatch);
	spriteBatch = batch;
	spriteBatch->getStateBlock()->setDepthWrite(false);
	spriteBatch->getStateBlock()->setDepthTest(true); // Render texture behind other images
	spriteBatch->getStateBlock()->setBlend(true);
	spriteBatch->getStateBlock()->setBlendSrc(RenderState::BLEND_SRC_ALPHA);
	spriteBatch->getStateBlock()->setBlendDst(RenderState::BLEND_ONE_MINUS_SRC_ALPHA);
}

/**
 * \brief Is called by the particle system. Will check if particles can be emitted. Will emit 
 *		  particles based on particlesPerEmit.	
 * \param amount 
 */
void ParticleSystem::EmitParticles(int amount)
{
	if (particleCount >= MAX_PARTICLES) return;
	const int particlesToEmit = particleCount + amount;
	for (unsigned int i = particleCount; i < particlesToEmit; i++)
	{
		Particle* p = &particles[i];

		// Calculate direction
		const float rndX = Utilities::RandomNumber(-0.01f, 0.01f);
		const float rndY = Utilities::RandomNumber(0, 0.1f);
		const float rndZ = Utilities::RandomNumber(-0.01f, 0.01f);
		Vector3 direction = Vector3(rndX, rndY, rndZ);

		// Calculate particle speed
		const float randomSpeed = Utilities::RandomNumber(0.05f, 0.1f);
		direction.normalize();
		direction *= randomSpeed;

		// Set particles properties
		p->position = particleSystemNode->getTranslation();
		p->velocity = direction;
		p->color = Vector4(Utilities::RandomNumber(0, 1.0f), Utilities::RandomNumber(0, 1.0f), Utilities::RandomNumber(0, 1.0f), 1);
		p->age = 0.0f;
		p->maxLifeTime = particleLifeTime;

		particleCount++;
	}
}


// ReSharper restore CppUseAuto
