#include "Particle.h"
#include "../Utilities/Utilities.h"

Particle::Particle()
{
	
}
Particle::Particle(Vector3 pos, Vector3 startVel, Vector4 startCol, float lifeTime) : mass(1), position(pos), velocity(startVel), color(startCol),
age(0.0f), angle(0), maxLifeTime(lifeTime)
{
	Utilities::PrintToOutputWindow("My x = "); Utilities::PrintToOutputWindow(position.x);
	age = 0;
}

bool Particle::Update(float elapsedTime)
{
	// TODO: Apply envirnoment forces to velocity
	position = position + velocity;
	age = age + elapsedTime;
	return IsAlive();
}

/**
 * \brief Will check if particle is still alive.
 * \return 
 */
bool Particle::IsAlive() const
{
	return age < maxLifeTime;
}
Vector3 Particle::Position()
{
	return position;
}

Vector3 Particle::Velocity()
{
	return velocity;
}

Vector4 Particle::Color()
{
	return color;
}

float Particle::Age()
{
	return age;
}
