#pragma once
#ifndef PARTICLE_H
#define PARTICLE_H
#include <gameplay.h>

using namespace gameplay;

class Particle
{	
public:
	Particle();
	Particle(Vector3 pos, Vector3 startVel, Vector4 startCol, float lifeTime);
	float mass;
	float age;
	float angle;
	float maxLifeTime;
	Vector3 position;
	Vector3 velocity;
	Vector4 color;
	bool Update(float elapsedTime);
	bool IsAlive() const;
	Vector3 Position();
	Vector3 Velocity();
	Vector4 Color();
	float Age();
};
#endif

