#include "GolfyGolfersMain.h"

// Declare our game instance
GolfyGolfersMain game;

GolfyGolfersMain::GolfyGolfersMain()
	: showScoreMenu(false), distance(0), font(nullptr), ballNode(nullptr), rotateX(0), deltaX(0), Wireframe_(false),
	Menu_(nullptr)
{
}

void GolfyGolfersMain::initialize()
{
	GameManager::GetInstance()->onInGameMenu = false;
	GameManager::GetInstance()->onMenu = true;
	Menu_ = Form::create("res/ui/menu.form");
	Lobby_ = Form::create("res/ui/lobby.form");
	inGameMenu = Form::create("res/ui/ingamemenu.form");
	inGameUI = Form::create("res/ui/ingameUI.form");
	inGameUIPower = Form::create("res/ui/ingameUIPower.form");
	Lobby_->setEnabled(false);
	inGameMenu->setEnabled(false);
	inGameUI->setEnabled(false);
	inGameUIPower->setEnabled(false);
	font = Font::create("res/fonts/arial.gpb");

	dynamic_cast<Button*>(Lobby_->getControl("readyButton"))->addListener(this, Control::Listener::RELEASE);
	dynamic_cast<Button*>(Lobby_->getControl("disconnectLobbyButton"))->addListener(this, Control::Listener::RELEASE);
	dynamic_cast<Button*>(Menu_->getControl("singlePlayerButton"))->addListener(this, Control::Listener::RELEASE);
	dynamic_cast<Button*>(Menu_->getControl("multiPlayerButton"))->addListener(this, Control::Listener::RELEASE);
	dynamic_cast<Button*>(Menu_->getControl("exitButton"))->addListener(this, Control::Listener::RELEASE);
	dynamic_cast<Button*>(inGameMenu->getControl("disconnectPlayerButton"))->addListener(this, Control::Listener::RELEASE);

}

void GolfyGolfersMain::finalize()
{
	SAFE_RELEASE(GameManager::GetInstance()->scene);
	SAFE_RELEASE(font);
}

void GolfyGolfersMain::update(const float elapsedTime)
{
	auto instance = GameManager::GetInstance();

	if (instance->isLoadingFirstLevel)
	{
		LeaveLobby();
		instance->isLoadingFirstLevel = false;
	}

	if (instance->isLoadingNextLevel)
	{
		pause();
		instance->NextLevel();
		instance->isLoadingNextLevel = false;
		resume();
	}

	if (instance->isLoadingMenu)
	{
		pause();
		GameManager::GetInstance()->onMenu = true;
		Menu_->setEnabled(true);
		GameManager::GetInstance()->mNetwork->Disconnect();
		GameManager::GetInstance()->DeleteAllPlayers();
		GameManager::GetInstance()->DeleteSceneNodes(GameManager::GetInstance()->scene->getFirstNode());
		instance->isLoadingMenu = false;
		setCursorVisible(true);
		resume();
	}

	if (GameManager::GetInstance()->onLobby)
		Lobby_->update(elapsedTime);

	if (!instance->onMenu && !GameManager::GetInstance()->onLobby) {
		inGameUIPower->setWidth(GameManager::GetInstance()->ballPower);
		inGameUI->update(elapsedTime);
		inGameUIPower->update(elapsedTime);
		instance->player->Update(elapsedTime);
		if (instance->sand != nullptr) instance->sand->Update(elapsedTime);
		Menu_->update(elapsedTime);
		if (SceneManager::GetInstance() != nullptr)
			if (SceneManager::GetInstance()->puttHandler != nullptr)
			{
				SceneManager::GetInstance()->puttHandler->Update(elapsedTime);
			}
	}
	if (!instance->onMenu && !GameManager::GetInstance()->onLobby) instance->player->Update(elapsedTime);
	Menu_->update(elapsedTime);
	if (instance->onInGameMenu)
	{
		inGameMenu->update(elapsedTime);
	}
}

void GolfyGolfersMain::render(float elapsedTime)
{
	// Clear the color and depth buffers
	clear(CLEAR_COLOR_DEPTH, Vector4::zero(), 1.0f, 0);

	char shots[10] = "Shots: ";
	char shotsString[10];
	sprintf_s(shotsString, "%u", GameManager::GetInstance()->score);
	strcat_s(shots, shotsString);
	// Visit all the nodes in the scene for drawing
	if (!GameManager::GetInstance()->onMenu && GameManager::GetInstance()->scene && !GameManager::GetInstance()->onLobby) {
		GameManager::GetInstance()->scene->visit(this, &GolfyGolfersMain::DrawScene);
		inGameUI->draw();
		inGameUIPower->draw();
	}

	if (GameManager::GetInstance()->onMenu)
		Menu_->draw();
	else
	{
		font->start();
		if (!GameManager::GetInstance()->onMenu && GameManager::GetInstance()->scene && !GameManager::GetInstance()->onLobby) 
		{
			font->drawText("Power", getWidth() * 0.16, getHeight() * 0.87, Vector4(1, 0, 0, 1), 24);
			font->drawText(shots, getWidth() * 0.75, getHeight() * 0.90, Vector4(1, 0, 0, 1), 24);
		}

		if (GameManager::GetInstance()->onLobby)
		{
			char lobbyMenu[1024] = "";
			Lobby_->draw();
			strcat_s(lobbyMenu, CreateLobbyMenu(&GameManager::GetInstance()->opponents));
			font->drawText(lobbyMenu, getWidth() / 2, getHeight() / 4, Vector4(1, 0, 0, 1), 26);
		}

		if (showScoreMenu)
		{
			char scoreMenu[1024] = "";
			strcat_s(scoreMenu, CreateScoreMenu(&GameManager::GetInstance()->opponents));
			font->drawText(scoreMenu, getWidth() / 8, getHeight() / 8, Vector4(0, 0, 1, 1), 40);
		}
		if (GameManager::GetInstance()->onInGameMenu)
			inGameMenu->draw();
		font->finish();
		GameManager::GetInstance()->Render(elapsedTime);
	}
}

void GolfyGolfersMain::LeaveLobby()
{
	//GameManager::GetInstance()->onLobby = false;
	GameManager::GetInstance()->Initialize();
	inGameUI->setEnabled(true);
	inGameUIPower->setEnabled(true);
}

bool GolfyGolfersMain::DrawScene(Node* node)
{
	// If the node visited contains a drawable object, draw it
	auto drawable = node->getDrawable();
	if (drawable)
		drawable->draw(Wireframe_);

	return true;
}

const char * GolfyGolfersMain::CreateScoreMenu(std::vector<Opponents*>* opponents) const
{
	char scoreMenu[1024] = "";

	strcat_s(scoreMenu, "\t");
	for (auto i = 0; i < LEVELCOUNT; i++)
	{
		strcat_s(scoreMenu, std::to_string(i).c_str());
		strcat_s(scoreMenu, "\t");
	}
	strcat_s(scoreMenu, "\n");

	if (GameManager::GetInstance()->isMultiplayer)
	{
		for (auto& opponent : *opponents)
		{
			strcat_s(scoreMenu, std::to_string(opponent->id).c_str());
			for (auto j : opponent->score)
			{
				strcat_s(scoreMenu, "\t");
				strcat_s(scoreMenu, std::to_string(j).c_str());
			}
			strcat_s(scoreMenu, "\n");
		}
	}
	else
	{
		strcat_s(scoreMenu, "1");
		for (auto singlePlayerScore : GameManager::GetInstance()->singlePlayerScores)
		{
			strcat_s(scoreMenu, "\t");
			strcat_s(scoreMenu, std::to_string(singlePlayerScore).c_str());
		}
	}

	return scoreMenu;
}

const char * GolfyGolfersMain::CreateLobbyMenu(std::vector<Opponents*>* opponents) const
{
	char lobbyMenu[1024] = "";

	for (auto& opponent : *opponents)
	{
		strcat_s(lobbyMenu, std::to_string(opponent->id).c_str());
		strcat_s(lobbyMenu, "\n");
	}
	return lobbyMenu;
}

void GolfyGolfersMain::keyEvent(const Keyboard::KeyEvent evt, const int key)
{
	if (!GameManager::GetInstance()->onMenu && !GameManager::GetInstance()->onLobby) GameManager::GetInstance()->player->OnKeyboardInput(evt, key);
	if (evt == Keyboard::KEY_PRESS)
	{
		switch (key)
		{
		case Keyboard::KEY_R:
			{
			if (!GameManager::GetInstance()->onMenu && !GameManager::GetInstance()->onLobby) 
				GameManager::GetInstance()->player->GetGolfBall()->ResetToStartPosition();
			break;
			}
		case Keyboard::KEY_TAB:
			if (!showScoreMenu)
			{
				GameManager::GetInstance()->RequestScores();
			}
			showScoreMenu = true;
			break;
		case Keyboard::KEY_ESCAPE:
			if (!GameManager::GetInstance()->onMenu && !GameManager::GetInstance()->onLobby) {
				GameManager::GetInstance()->onInGameMenu = !GameManager::GetInstance()->onInGameMenu;
				inGameMenu->setEnabled(GameManager::GetInstance()->onInGameMenu);
				setCursorVisible(GameManager::GetInstance()->onInGameMenu);
			}
			break;
		default:
			break;
		}
	}
	if (evt == Keyboard::KEY_RELEASE)
	{
		switch (key)
		{
		case Keyboard::KEY_TAB:
			showScoreMenu = false;
			break;
		default:
			break;
		}
	}
}


void GolfyGolfersMain::touchEvent(const Touch::TouchEvent evt, const int x, const int y, const unsigned int contactIndex)
{
	if (!GameManager::GetInstance()->onMenu && !GameManager::GetInstance()->onLobby) GameManager::GetInstance()->player->OnTouchInput(evt, x, y, contactIndex);
	switch (evt)
	{
	case Touch::TOUCH_PRESS:

		start = btVector3(x, y, 0);

		break;
	case Touch::TOUCH_RELEASE:

		if (distance > 0)
		{
			power = power + distance / 10;
		}

		distance = 0;

		break;
	case Touch::TOUCH_MOVE:
		if (!GameManager::GetInstance()->onMenu && !GameManager::GetInstance()->onLobby) {
			if (!GameManager::GetInstance()->player->GetGolfBall()->isRolling)
			{
				end = btVector3(x, y, 0);
				distance = btDistance(start, end);
				//inGameUIPower->setSize(distance, 50);
				
				//inGameUI.set
			}
		}

		break;
	};
}

void GolfyGolfersMain::controlEvent(Control * control, const EventType evt)
{
	switch (evt)
	{
	case Control::Listener::RELEASE:
		if (strcmp("readyButton", control->getId()) == 0)
		{
			GameManager::GetInstance()->mNetwork->SendInstructionToServer(networking::LOBBY_READY, "");
			setCursorVisible(false);
			Lobby_->setEnabled(false);
		}
		else if (strcmp("disconnectLobbyButton", control->getId()) == 0)
		{
			GameManager::GetInstance()->onLobby = false;
			GameManager::GetInstance()->onMenu = true;
			Menu_->setEnabled(true);
			Lobby_->setEnabled(false);

			GameManager::GetInstance()->mNetwork->Disconnect();
			GameManager::GetInstance()->DeleteAllPlayers();
		}
		else if (strcmp("singlePlayerButton", control->getId()) == 0)
		{
			// Single player button has been pressed.
			GameManager::GetInstance()->onMenu = false;
			GameManager::GetInstance()->Initialize();
			GameManager::GetInstance()->isMultiplayer = false;
			Menu_->setEnabled(false);
			GameManager::GetInstance()->UpdatePlayers(0, true, 0);
			setCursorVisible(false);
			break;
		}
		else if (strcmp("multiPlayerButton", control->getId()) == 0)
		{
			// Multiplayer button has been pressed.
			GameManager::GetInstance()->onMenu = false;
			GameManager::GetInstance()->onLobby = true;

			auto bundle = Bundle::create("res/sphere.gpb");
			GameManager::GetInstance()->ballPrefab = bundle->loadNode("sphere")->clone();
			SAFE_RELEASE(bundle);
			std::thread t(&networking::Network::JoinNetwork, GameManager::GetInstance()->mNetwork);
			t.detach();
			Menu_->setEnabled(false);
			Lobby_->setEnabled(true);
			GameManager::GetInstance()->isMultiplayer = true;
			//setCursorVisible(false);
			break;
		}
		else if (strcmp("exitButton", control->getId()) == 0)
		{
			// Exit button has been pressed.
			GameManager::GetInstance()->mNetwork->Disconnect();
			exit();
		}
		else if (strcmp("disconnectPlayerButton", control->getId()) == 0)
		{
			// Disconnect button in game has been pressed.
			GameManager::GetInstance()->onInGameMenu = false;
			inGameMenu->setEnabled(false);
			GameManager::GetInstance()->onMenu = true;
			Menu_->setEnabled(true);
			setCursorVisible(true);

			GameManager::GetInstance()->mNetwork->Disconnect();
			GameManager::GetInstance()->DeleteAllPlayers();
			GameManager::GetInstance()->DeleteSceneNodes(GameManager::GetInstance()->scene->getFirstNode());
		}
		break;
	default:
		break;
	}
}

bool GolfyGolfersMain::mouseEvent(const Mouse::MouseEvent evt, int x, int y, int wheelDelta)
{
	if (!GameManager::GetInstance()->onMenu && !GameManager::GetInstance()->onLobby) GameManager::GetInstance()->player->OnMouseEvent(evt, x, y, wheelDelta);
	if (evt == Mouse::MOUSE_PRESS_LEFT_BUTTON)
	{
		pressed = true;
	}
	if (evt == Mouse::MOUSE_RELEASE_LEFT_BUTTON)
	{
		pressed = false;
	}
	return false;
}