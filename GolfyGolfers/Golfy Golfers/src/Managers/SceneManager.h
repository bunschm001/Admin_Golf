#pragma once
#include "gameplay.h"
#include "../Misc/puttHandler.h"
#include "../Misc/PlayerClass.h"
#include "../Misc/SandHandler.h"

//#include "GameManager.h"

using namespace gameplay;

class SceneManager
{
public:
	static SceneManager* GetInstance()
	{
		if (!Instance_)
			Instance_ = new SceneManager();
		return Instance_;
	}
	SceneManager();

	PuttHandler* puttHandler{};
	SandHandler* sandHandler{};
	Node *ballPrefab{};
	Node* golfBall{};
	PlayerClass* player{};
	Scene* scene{};

	std::string levelName;

	Scene* LoadNextLevel(int level);
	bool InitializeScene(Node * node);


private:
	static SceneManager* Instance_;
protected:

};

