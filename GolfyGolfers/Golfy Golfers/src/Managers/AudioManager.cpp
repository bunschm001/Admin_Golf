#include "AudioManager.h"

AudioManager* AudioManager::Instance_;


AudioManager::AudioManager()
{
}


void AudioManager::LoadSounds()
{
	backgroundMusic = AudioSource::create("res/sounds/game.audio#background", true);
	if (backgroundMusic) backgroundMusic->play();
	

	ballHitSound = AudioSource::create("res/sounds/game.audio#ballHitSound", true);
	ballCollisionSound = AudioSource::create("res/sounds/game.audio#ballCollisionSound", true);
	puttSound = AudioSource::create("res/sounds/game.audio#puttSound", true);
}
