#include "SceneManager.h"
#include "GameManager.h"

SceneManager* SceneManager::Instance_;

/**
 * \brief the scenemanager is used to load new levels and cast them back to the game manager. It is made an instance because only one scene manager is allowed in the game
 */
SceneManager::SceneManager()
= default;



/**
 * \brief this function returns the new scene back to the game manager
 * \param level is needed to know which level to load next
 * \return returns the new scene
 */
Scene* SceneManager::LoadNextLevel(const int level)
{
	char intString[64]; // enough to hold all numbers up to 64-bits
	char path1[128] = "res/scenes/scene";
	char path2[128] = ".scene";

	char path[128] = "";
	sprintf_s(intString, "%d", level);
	strcat_s(path, path1);
	strcat_s(path, intString);
	strcat_s(path, path2);

	//loads the next scene
	scene = Scene::load(path);

	//modify all objects in the .scene file 
	auto floorNode = scene->findNode("floor");
	auto floorModel = dynamic_cast<Model*>(floorNode->getDrawable());
	const auto floorMesh = floorModel->getMesh();
	PhysicsRigidBody::Parameters rbParams(0.0, 0.5, 0.75, 0.025, 0.16);
	floorNode->setCollisionObject(PhysicsCollisionObject::RIGID_BODY, PhysicsCollisionShape::mesh(floorMesh), &rbParams);

	auto holeNode = scene->findNode("hole");
	auto holeModel = dynamic_cast<Model*>(holeNode->getDrawable());
	auto holeMaterial = holeModel->getMaterial();
	const auto holeMesh = holeModel->getMesh();
	holeNode->setCollisionObject(PhysicsCollisionObject::RIGID_BODY, PhysicsCollisionShape::mesh(holeMesh));
	scene->visit(this, &SceneManager::InitializeScene);

	auto sandNode = scene->findNode("sandPlane");
	if (sandNode) 
	{
		auto sandModel = dynamic_cast<Model*>(sandNode->getDrawable());
		auto sandMaterial = sandModel->getMaterial();
		const auto sandMesh = sandModel->getMesh();
		sandNode->setCollisionObject(PhysicsCollisionObject::GHOST_OBJECT, PhysicsCollisionShape::mesh(sandMesh));

		sandHandler = new SandHandler();
		GameManager::GetInstance()->sand = sandHandler;
		sandHandler->CheckColBetween(GameManager::GetInstance()->ballPrefab, sandNode);

		scene->addNode(sandNode);
	}	
	auto wallNode = scene->findNode("walls");

	if (wallNode)
	{
		auto wallModel = dynamic_cast<Model*>(wallNode->getDrawable());
		auto wallMaterial = wallModel->getMaterial();
		const auto wallMesh = wallModel->getMesh();
		PhysicsRigidBody::Parameters wallParams(0.0, 0.5, 1, 0.025, 0.16);

		wallNode->setCollisionObject(PhysicsCollisionObject::RIGID_BODY, PhysicsCollisionShape::mesh(wallMesh), &wallParams);


		scene->addNode(wallNode);
	}
	return scene;
}

bool SceneManager::InitializeScene(Node * node)	
{
	static Node* lightNode = scene->findNode("directionalLight1");

	Model* model = dynamic_cast<Model*>(node->getDrawable());

	if (model)
	{
		Material* material = model->getMaterial();
		const auto mesh = model->getMesh();
		if (node->getCollisionObject())
		{
			float massValue = 0;
			float frictionValue = 0.5;
			float restitutionValue = 0.35;
			float linearDampingValue = 1;
			float angularDampingValue = 1;
			if(node == scene->findNode("walls"))
			{
				restitutionValue = 1;
				float frictionValue = 0;
			}
			PhysicsRigidBody::Parameters rbParams(massValue, frictionValue, restitutionValue, linearDampingValue, angularDampingValue);
			node->setCollisionObject(PhysicsCollisionObject::RIGID_BODY, PhysicsCollisionShape::mesh(mesh), &rbParams);
		}

		if (material && material->getTechnique()->getPassByIndex(0)->getEffect()->getUniform("u_directionalLightDirection"))
		{
			material->getParameter("u_ambientColor")->setValue(scene->getAmbientColor());
			//material->getParameter("u_directionalLightColor[0]")->setValue(lightNode->getLight()->getColor());
			//material->getParameter("u_directionalLightDirection[0]")->setValue(lightNode->getForwardVectorView());
		}

		if (node == scene->findNode("hole"))
		{
			puttHandler = new PuttHandler();
			puttHandler->CheckColBetween(GameManager::GetInstance()->ballPrefab, node);
		}

		if (node == scene->findNode("sandPlane"))
		{
			if (node)
			{				
				sandHandler = new SandHandler();
				GameManager::GetInstance()->sand = sandHandler;
				sandHandler->CheckColBetween(GameManager::GetInstance()->ballPrefab, node);

				scene->addNode(node);
			}
		}
	}
	return true;
}