#pragma once
#include <AudioSource.h>

using namespace gameplay;

class AudioManager
{
public:
	static AudioManager* GetInstance()
	{
		if (!Instance_)
			Instance_ = new AudioManager();
		return Instance_;
	}

	AudioManager();

	void LoadSounds();

	AudioSource* backgroundMusic;
	AudioSource* ballHitSound;
	AudioSource* ballCollisionSound;
	AudioSource* puttSound;
	
private:
	static AudioManager* Instance_;
};
