#include "GameManager.h"

GameManager* GameManager::Instance_;

/**
* \brief The GameManager is a class which handles most of the the game. It is made an instance because only one game manager is allowed in the game
*/
GameManager::GameManager()
{
	sand = nullptr;
	mNetwork = new networking::Network();
	isLoadingFirstLevel = false;
	isLoadingNextLevel = false;
}


/**
* \brief Initialize is called the first time a player starts a game. The bundle is used to create a prefab of the golfBall in order to later use this prefab for multiple purposes.
*/
void GameManager::Initialize() {
	auto bundle = Bundle::create("res/sphere.gpb");
	ballPrefab = bundle->loadNode("sphere")->clone();
	SAFE_RELEASE(bundle);

	AudioManager::GetInstance()->LoadSounds();
	level = 0;
	NextLevel();
}


/**
 * \brief Will be called in de render function in the Main class. Is used to render objects.
 * \param elapsedTime
 */
void GameManager::Render(float elapsedTime)
{
	for (std::list<ParticleSystem*>::const_iterator iter = particleSystems.begin(),
		end = this->particleSystems.end();
		iter != end;
		++iter)
	{
		(*iter)->Update(elapsedTime);
		(*iter)->Draw();
	}
}

/**
* \brief this function is used when a player disconnects from the server. All other clients recieves a message and via this function the correct corresponding ball is deleted.
* \param id is used to link the correct id to the correct ball in order to delete the correct corresponding ball
*/
void GameManager::DeletePlayer(const int id)
{
	for (auto i = 0; i < opponents.size(); i++)
		if (opponents[i]->id == id)
		{
			if (!onMenu && !onLobby)
			{
				GameManager::GetInstance()->scene->removeNode(opponents[i]->ballNode);
				opponents[i]->ballNode->removeAllChildren();
				opponents[i]->ballNode->getCollisionObject()->setEnabled(false);
			}

			opponents.erase(opponents.begin() + i); //this makes sure that the opponent that left is deleted from the opponents vector(list).
			return;
		}
}

/**
* \brief this function is used between a level switch. By deleting all players' balls, we can reinstanciate them when loading the next level.
*/
void GameManager::DeleteAllPlayers()
{
	for (auto& opponent : opponents)
	{
		if (opponent->id != ownId && !onMenu && !onLobby)
		{
			GameManager::GetInstance()->scene->removeNode(opponent->ballNode);
			opponent->ballNode->getCollisionObject()->setEnabled(false);
			opponent->ballNode->removeAllChildren();
		}
	}
	opponents.clear();
}



/**
* \brief Requests an overview of all players in the lobby.
*/
void GameManager::RequestPlayers()
{
	mNetwork->SendInstructionToServer(networking::UPDATE_PLAYERS, "");
}

/**
* \brief Updates the status of other players
*
* @param id, isPlaying, score is needed to place the status and score to the correct ID
*
*/
void GameManager::UpdatePlayers(const int id, const bool isPlaying, const int score)
{
	for (auto& opponent : opponents)
		if (opponent->id == id)
		{
			opponent->isPlaying = isPlaying;
			opponent->score[level] = score;
			return; //stops the function when a player is updated
		}
	if (ownId != id)
	{
		AddNewPlayer(id, isPlaying); //spawns a new player if the ID is not the same as our own ID
	}
	else
	{
		const auto opponent = new Opponents();
		opponent->id = id;
		opponent->isPlaying = isPlaying;
		opponents.push_back(opponent);
	}

};

/**
* \brief Recieves the position of other players
*
* @param id, pos is needed to place the position to the correct ball
*
*/
void GameManager::UpdatePosOfPlayers(const int id, const Vector3 pos) {
	for (auto& opponent : opponents)
		if (opponent->id == id)
		{
			opponent->ballNode->setTranslation(pos);
			return;
		}
}

/**
* \brief Recieves the impulse and id to apply the same impulse to the opponents ball
*
* @param id, impulse is needed to give an impulse to the correct ball
*
*/
void GameManager::RecieveBallForce(const int id, const Vector3& impulse) {
	for (auto& opponent : opponents)
		if (opponent->id == id)
		{
			opponent->ball->applyImpulse(impulse);
			return;
		}

}

/**
* \brief Recieves the ending position of the opponents to dodge small mistakes in end position between this client and other clients
*
* @param id, pos is needed to place the status to the correct ID
*
*/
void GameManager::RecieveBallEndPos(const int id, const Vector3& pos) {
	for (auto& opponent : opponents)
		if (opponent->id == id)
		{
			opponent->ballNode->setTranslation(pos);
			return;
		}
}

/**
* \brief Recieves the scores of other players from server.
*
* @param scoreData is a list of ids combined with their scores, needed to update the score list locally.
*
*/
void GameManager::UpdateScores(std::vector<int> scoreData)
{
	const int setAmount = (scoreData.size() - 2) / (LEVELCOUNT + 1);

	for (auto i = 0; i < setAmount; i++)
	{
		if (opponents[i]->id == scoreData[i * (LEVELCOUNT + 1) + 2])
		{
			for (auto j = 0; j < LEVELCOUNT; j++)
				opponents[i]->score[j] = scoreData[i * (LEVELCOUNT + 1) + 3 + j];
		}
	}
}

/**
* \brief Recieves the status of other players if they've finished the course
*
* @param stateData
*
*/
void GameManager::UpdateStates(std::vector<int> stateData)
{
	auto allDone = false;
	const int setAmount = (stateData.size() - 2) / 2;
	for (auto i = 0; i < setAmount; i++)
	{
		for (auto j = 0; j < setAmount; j++)
		{
			if (opponents[j]->id == stateData[2 + j * 2])
			{
				opponents[j]->isPlaying = stateData[3 + j * 2];
			}
		}
	}

	for (auto& opponent : opponents)
	{
		if (opponent->isPlaying || !player->putted)
		{
			allDone = false;
			break;
		}
		else
			allDone = true;
	}


	if (allDone)
	{
		if (level < 2)
		{
			level++;
			isLoadingNextLevel = true;
		}
		else
		{
			if (!onLobby && !onMenu) isLoadingMenu = true;
		}
		
	}
}

/**
* \brief Recieves and updates the id of this player.
*
* @param id is needed to update to the correct ID
*
*/
void GameManager::UpdateId(const int id)
{
	ownId = id;
}


/**
* \brief Sends score to server
*
*/
void GameManager::SaveScore() const
{
	auto _level = level - 1;
	char packetData[8] = "";
	strcat_s(packetData, std::to_string(score).c_str());
	strcat_s(packetData, ",");
	strcat_s(packetData, std::to_string(level).c_str());
	mNetwork->SendInstructionToServer(networking::SAVE_SCORE, packetData);
}

/**
 * \brief sends the state to the server
 * \param isPlaying
 */
void GameManager::SaveState(const bool isPlaying) const
{
	char packetData[2] = "";
	strcat(packetData, isPlaying ? "1" : "0");
	mNetwork->SendInstructionToServer(networking::SAVE_STATE, packetData);
}

/**
 * \brief Requests all the scores of the opponents from the server
 */
void GameManager::RequestScores()
{
	GameManager::GetInstance()->mNetwork->SendInstructionToServer(networking::UPDATE_SCORE, "");
}

/**
 * \brief Requests all the states of the opponents from the server
 */
void GameManager::RequestStates()
{
	GameManager::GetInstance()->mNetwork->SendInstructionToServer(networking::UPDATE_STATES, "");
}

/**
 * \brief This function is used to spawn new players, creating their balls and adding them to the opponents list.
 * \param id is needed to apply it to the correct ball
 * \param isPlaying is needed to set the players state to active or inactive
 */

void GameManager::AddNewPlayer(const int id, const bool isPlaying)
{
	char path0[128] = "";
	strcat(path0, "res/demo.material#lambertBall");
	strcat(path0, std::to_string(id).c_str());
	
	auto opponent = new Opponents();
	opponent->id = id;
	opponent->isPlaying = isPlaying;

	if (!onMenu && !onLobby)
	{
		opponent->ballNode = ballPrefab->clone();

		opponent->ballModel = dynamic_cast<Model*>(opponent->ballNode->getDrawable());
		opponent->ballMaterial = opponent->ballModel->getMaterial();
		dynamic_cast<Model*>(opponent->ballNode->getDrawable())->setMaterial(path0, 0);

		opponent->ballNode->setScale(1, 1, 1);
		opponent->ballNode->setTranslation(startPos);
		PhysicsRigidBody::Parameters rbParams(1, 1, 1, 0.3f, 0.3f);
		opponent->ballNode->setCollisionObject(PhysicsCollisionObject::RIGID_BODY, PhysicsCollisionShape::sphere(0.5f), &rbParams, 8, 1);
		opponent->ball = dynamic_cast<PhysicsRigidBody*>(opponent->ballNode->getCollisionObject());
		scene->addNode(opponent->ballNode);
	}

	opponents.push_back(opponent);
}

/**
 * \brief This function is used to switch to the next level. First all nodes in the current scene are deleted,
 * then the new scene is loaded and after that we create a new PlayerClass for the local player and we request the information of our opponents from the server
 */
void GameManager::NextLevel() {
	DeleteAllPlayers();
	if (level != 0)
	{
		
		DeleteSceneNodes(scene->getFirstNode());
	}
	particleSystems.clear();
	scene = SceneManager::GetInstance()->LoadNextLevel(level);
	startPos = static_cast<Vector3>(scene->findNode("golfBall")->getTranslation()); //update the startpos to the new startpos of the new level

	scene->findNode("golfBall")->getCollisionObject()->setEnabled(false);
	scene->removeNode(scene->findNode("golfBall"));

	player = new PlayerClass(GameManager::GetInstance()->scene->getActiveCamera()->getNode(), GameManager::GetInstance()->ballPrefab->clone());

	player->putted = false;
	score = 0;

	RequestPlayers();
	RequestScores();
	SaveState(true);
	playerCam = player->GetCamera();
	if (onLobby) onLobby = false;
}

/**
 * \brief this is a recursive function that deletes all the nodes from a scene
 * \param startNode is needed to know what node we currently are deleting and which node to delete after that.
 */
void GameManager::DeleteSceneNodes(Node* startNode)
{
	if (startNode->getChildCount() > 0) startNode->removeAllChildren();

	if (startNode->getNextSibling())GameManager::DeleteSceneNodes(startNode->getNextSibling());

	GameManager::GetInstance()->scene->removeNode(startNode);
}


/**
 * \brief Adds a particly system to the particle system list.
 * \param pSystem
 */
void GameManager::AddParticleSystem(ParticleSystem* pSystem)
{
	particleSystems.push_front(pSystem);
}

void GameManager::RemoveParticleSystem(ParticleSystem* pSystem)
{
	// TODO: Able to delete particly systems by unique id/ object reference/ == operator
	//particleSystems.remove_if(pSystem);
}

