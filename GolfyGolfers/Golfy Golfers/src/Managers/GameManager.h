#include <thread>
#include <vector>
#include "gameplay.h"
#include "../Utilities/Utilities.h"
#include "../Networking/Network.h"
#include "SceneManager.h"
#include "../Misc/PlayerClass.h"
#include "../Particle System/ParticleSystem.h"
#include "../Misc/SandHandler.h"
#include "AudioManager.h"

#define LEVELCOUNT 18
#define BALLSTOPSPEED 0.00001f

using namespace gameplay;

struct Opponents {
	int id;
	bool isPlaying;
	int score[LEVELCOUNT];

	gameplay::Node *ballNode;
	Model* ballModel;
	Material* ballMaterial;
	gameplay::PhysicsRigidBody *ball;
};

class GameManager
{
public:
	static GameManager* GetInstance() 
	{
		if (!Instance_)
			Instance_ = new GameManager();
		return Instance_;
	}
	bool isLoadingFirstLevel;
	bool isLoadingNextLevel;
	bool isLoadingMenu = false;
	bool onMenu{};
	bool onLobby{};
	bool onInGameMenu{false};
	bool isMultiplayer{};
	int ownId{};
	networking::Network* mNetwork;
	std::vector<Opponents*> opponents;
	gameplay::Vector3 startPos;
	int level = 0;
	int score = 0;
	int ballPower = 0;
	int singlePlayerScores[18] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; // Very ugly, yes, it's temporary.
	Vector3 startPosArray[10]; //max 10 players

	gameplay::Scene* scene{};
	Node *ballPrefab{};
	PlayerClass* player{};
	PlayerCamera* playerCam{};
	std::list<ParticleSystem*> particleSystems;
	SandHandler* sand;

	GameManager();
	void Render(float elapsedTime);
	void RequestPlayers();
	void UpdatePlayers(int id, bool isPlaying, int score);
	void UpdatePosOfPlayers(int id, Vector3 pos);
	void RecieveBallForce(int id, const Vector3& impulse);
	void RecieveBallEndPos(int id, const Vector3& pos);
	void UpdateScores(std::vector<int> scoreData);
	void UpdateStates(std::vector<int> stateData);
	void UpdateId(int id);
	void SaveScore() const;
	void SaveState(bool isPlaying) const;
	static void RequestScores();
	static void RequestStates();
	void FillStartPos(int);
	void Initialize();
	void DeletePlayer(int id);
	void DeleteAllPlayers();
	static void DeleteSceneNodes(Node* startNode);
	void NextLevel();
	void AddParticleSystem(ParticleSystem* pSystem);
	void RemoveParticleSystem(ParticleSystem* pSystem);


private:
	static GameManager* Instance_;		
	void AddNewPlayer(int id, bool isPlaying);

};

