#pragma once

#include <thread>
#include <queue>
#include "enet/enet.h"

#define SERVER_ADDRESS "80.127.228.175"
#define PORT 1248

namespace networking
{
	
	enum NETWORK_INSTRUCTION_TYPE
	{
		VALIDATE = 0,
		UPDATE_PLAYERS = 1,
		REMOVE_PLAYER = 2,
		SAVE_SCORE = 3,
		SAVE_STATE = 4,
		APPLY_IMPULSE = 5,
		UPDATE_FORCE = 6,
		UPDATE_SCORE = 7,
		UPDATE_POSITION = 8,
		UPDATE_STATES = 9,
		LOBBY_READY = 10,
	};

	class Network
	{
	public:
		std::queue<const std::string*> instructions;
	private:
		bool Active_ = false;
		ENetAddress Address_{};
		ENetHost *Client_;
		ENetPeer *Peer_{};
	public:
		Network();
		void JoinNetwork();
		void Disconnect();
		void SendInstructionToServer(NETWORK_INSTRUCTION_TYPE, const char *) const;
	private:
		void ReceivePacketOnClient(const char * packetData) const;
	};

}