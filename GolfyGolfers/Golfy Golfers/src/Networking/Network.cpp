#include "../Managers/GameManager.h"
#include "Network.h"

namespace networking
{
	/**
	* This constructor will initialize the ENet library for network connections.
	*/
	Network::Network() : Client_(nullptr)
	{
		// Initialize the ENet library
		if (enet_initialize() != 0)
			Utilities::PrintToOutputWindow("An error occured while initializing ENet.\n");
		else
		{
			Utilities::PrintToOutputWindow("Succesfully initialized ENet.\n");
			atexit(enet_deinitialize);
		}
	}

	/**
	* This function, that needs to be run in a detached thread, will add a client on the network & the network object will now behave as a client.
	*/
	void Network::JoinNetwork()
	{
		ENetEvent event;

		atexit(enet_deinitialize);

		Client_ = enet_host_create(nullptr, 1, 2, 57600 / 8, 14400 / 8);

		if (Client_ == nullptr) {
			Utilities::PrintToOutputWindow("An error occured while trying to create an ENet server host\n");
			exit(EXIT_FAILURE);
		}

		// Here the client is connected to the server based on an ip-address.
		enet_address_set_host(&Address_, SERVER_ADDRESS);
		Address_.port = PORT;

		Peer_ = enet_host_connect(Client_, &Address_, 2, 0);

		Utilities::PrintToOutputWindow("Succesfully created a client host.\n");

		Active_ = true;
		auto eventStatus = 1;

		while (true) {

			// This service function will handle network events using a switch statement.
			eventStatus = enet_host_service(Client_, &event, 10000000);

			if (eventStatus > 0) {
				switch (event.type) {
				case ENET_EVENT_TYPE_CONNECT:
				{
					// This will happen when a peer, in this case just the server, connects with this client.
					Utilities::PrintToOutputWindow("(CLIENT) CONNECT, HOST: ");
					Utilities::PrintToOutputWindow(event.peer->address.host);
					Utilities::PrintToOutputWindow("\n");
					break;
				}
				case ENET_EVENT_TYPE_RECEIVE:
					// This will happen when a packet is received, and now it must be processed using ReceivePacketOnCLient.
					Utilities::PrintToOutputWindow("(CLIENT) RECEIVE, MESSAGE: \"");
					Utilities::PrintToOutputWindow(reinterpret_cast<const char *>(event.packet->data));
					Utilities::PrintToOutputWindow("\" FROM: ");
					Utilities::PrintToOutputWindow(event.peer->address.host);
					Utilities::PrintToOutputWindow("\n");
					ReceivePacketOnClient(reinterpret_cast<const char*>(event.packet->data));
					break;

				case ENET_EVENT_TYPE_DISCONNECT:
					// This will happen when a peer, in this case just the server, disconnects from this client.
					Utilities::PrintToOutputWindow("(CLIENT) DISCONNECT, DATA: ");
					Utilities::PrintToOutputWindow(reinterpret_cast<int>(event.peer->data));
					Utilities::PrintToOutputWindow("\n");
					event.peer = nullptr;
					break;
				default:
					break;
				}
			}
		}
	}

	/**
	* This function disconnects the client from the server.
	*/
	void Network::Disconnect()
	{
		if (Active_)
		{
			enet_peer_disconnect_now(Peer_, NULL);
			Active_ = true;
		}
	}

	/**
	* This function creates a packet based on the parameters and sends it to the clients peer, in this case the server.
	*
	* @param instructionType lets the network know what to do with the instruction data.
	* @param instructionData the data that needs to be sent over the network which will be processed based on the given instruction type.
	*
	*/
	void Network::SendInstructionToServer(const NETWORK_INSTRUCTION_TYPE instructionType, const char * instructionData) const
	{
		// Combine the packet.
		if (!Active_) return;
		char packetData[64];
		sprintf_s(packetData, sizeof(packetData), "%d", instructionType);
		strcat_s(packetData, ",");
		strcat_s(packetData, instructionData);

		// Send it it to the server.
		const auto packet = enet_packet_create(packetData, strlen(packetData) + 1, ENET_PACKET_FLAG_RELIABLE);
		enet_peer_send(Peer_, 0, packet);
		enet_host_flush(Client_);
	}

	/**
	* This function handles receiving a packet on the client by processing it and deciding what to do with it based on instruction type.
	*
	* @param packetData the data from a packet received from the network.
	*
	*/
	void Network::ReceivePacketOnClient(const char * packetData) const
	{
		// Divide the packet
		auto dividedPacketData = Utilities::DivideString(std::string(packetData), ',');
		const auto id = Utilities::StringToInt(dividedPacketData[0]);
		const auto instructionType = Utilities::StringToInt(dividedPacketData[1]);

		// Based on the instruction type, make it process the data.
		switch (instructionType)
		{
		case VALIDATE:
		{
			// Reply to the servers validation request.
			const auto packet = enet_packet_create("0,0,", strlen("0,0,") + 1, ENET_PACKET_FLAG_RELIABLE);
			enet_peer_send(Peer_, 0, packet);
			enet_host_flush(Client_);
			break;
		}
		case UPDATE_PLAYERS:
		{
			// Update the id's of all opponent balls instruction.
			GameManager::GetInstance()->UpdateId(atoi(dividedPacketData[2].c_str()));
			GameManager::GetInstance()->opponents.clear();
			for (auto i = 3; i < dividedPacketData.size(); i++)
				GameManager::GetInstance()->UpdatePlayers(Utilities::StringToInt(dividedPacketData[i]), true, 0);
			GameManager::GetInstance()->RequestStates();
			GameManager::GetInstance()->RequestScores();
			break;
		}
		case REMOVE_PLAYER:
		{
			// Remove disconnected player from scene.
			GameManager::GetInstance()->DeletePlayer(atoi(dividedPacketData[2].c_str()));
			break;
		}
		case SAVE_SCORE:
		{
			// Nothing happens here. This should never be called, but it would not break anything if it did happen.
			break;
		}
		case UPDATE_SCORE:
		{
			// Recieves the scores of other players from server.
			GameManager::GetInstance()->UpdateScores(Utilities::StringVectorToIntVector(dividedPacketData));
			break;
		}
		case UPDATE_FORCE:
		{
			// Update force of opponent ball with given id instruction.
			GameManager::GetInstance()->RecieveBallForce(id, Vector3(Utilities::CharToFloat(dividedPacketData[2].c_str()), Utilities::CharToFloat(dividedPacketData[3].c_str()), Utilities::CharToFloat(dividedPacketData[4].c_str())));
			break;
		}
		case UPDATE_POSITION:
		{
			// Update position of opponent ball with given id instruction.
			GameManager::GetInstance()->RecieveBallEndPos(id, Vector3(Utilities::CharToFloat(dividedPacketData[2].c_str()), Utilities::CharToFloat(dividedPacketData[3].c_str()), Utilities::CharToFloat(dividedPacketData[4].c_str())));
			break;
		}
		case UPDATE_STATES:
		{
			// Update statces of opponent ball with given id instruction.
			GameManager::GetInstance()->UpdateStates(Utilities::StringVectorToIntVector(dividedPacketData));
			break;
		}
		case LOBBY_READY:
			GameManager::GetInstance()->isLoadingFirstLevel = true;
			break;
		default:
			break;
		}
	}

}