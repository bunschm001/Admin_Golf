var hierarchy =
[
    [ "CollisionListener", null, [
      [ "PuttHandler", "d1/d06/class_putt_handler.html", null ]
    ] ],
    [ "Game", null, [
      [ "GolfyGolfersMain", "d5/d09/class_golfy_golfers_main.html", null ]
    ] ],
    [ "GameManager", "db/d10/class_game_manager.html", null ],
    [ "GolfBall", "de/d96/class_golf_ball.html", null ],
    [ "Listener", null, [
      [ "GolfyGolfersMain", "d5/d09/class_golfy_golfers_main.html", null ]
    ] ],
    [ "networking::Network", "de/dac/classnetworking_1_1_network.html", null ],
    [ "Opponents", "d9/d6a/struct_opponents.html", null ],
    [ "PlayerCamera", "d8/dcd/class_player_camera.html", null ],
    [ "PlayerClass", "d2/dd5/class_player_class.html", null ],
    [ "SceneManager", "d0/dc3/class_scene_manager.html", null ],
    [ "Spectate", "d2/d8c/class_spectate.html", null ],
    [ "Utilities", "db/db8/class_utilities.html", null ]
];