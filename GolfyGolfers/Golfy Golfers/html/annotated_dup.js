var annotated_dup =
[
    [ "networking", null, [
      [ "Network", "de/dac/classnetworking_1_1_network.html", "de/dac/classnetworking_1_1_network" ]
    ] ],
    [ "GameManager", "db/d10/class_game_manager.html", "db/d10/class_game_manager" ],
    [ "GolfBall", "de/d96/class_golf_ball.html", "de/d96/class_golf_ball" ],
    [ "GolfyGolfersMain", "d5/d09/class_golfy_golfers_main.html", "d5/d09/class_golfy_golfers_main" ],
    [ "Opponents", "d9/d6a/struct_opponents.html", "d9/d6a/struct_opponents" ],
    [ "PlayerCamera", "d8/dcd/class_player_camera.html", "d8/dcd/class_player_camera" ],
    [ "PlayerClass", "d2/dd5/class_player_class.html", "d2/dd5/class_player_class" ],
    [ "PuttHandler", "d1/d06/class_putt_handler.html", "d1/d06/class_putt_handler" ],
    [ "SceneManager", "d0/dc3/class_scene_manager.html", "d0/dc3/class_scene_manager" ],
    [ "Spectate", "d2/d8c/class_spectate.html", "d2/d8c/class_spectate" ],
    [ "Utilities", "db/db8/class_utilities.html", null ]
];